﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] float life;
    [SerializeField] float speed;
    [SerializeField] Rigidbody rigidbody;
    [SerializeField] Transform muzzle;
    [SerializeField] GameObject bullet;

    Transform player;

    void Start()
    {
        StartCoroutine(Move());
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    IEnumerator Move()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);

            this.transform.LookAt(new Vector3(player.position.x, this.transform.position.y, player.position.z), Vector3.up);
            this.transform.eulerAngles += new Vector3(0f, Random.Range(-50f, 50f), 0f);

            rigidbody.velocity = this.transform.forward * speed;

            yield return new WaitForSeconds(2f);

            this.transform.LookAt(new Vector3(player.position.x, this.transform.position.y, player.position.z), Vector3.up);

            GameObject go = Instantiate(bullet);
            go.tag = "EnemyBullet";
            go.transform.position = muzzle.position;
            go.GetComponent<Rigidbody>().velocity += muzzle.forward * 30f;
            Destroy(go, 5f);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != "PlayerBullet")
        {
            return;
        }

        Destroy(collision.collider.gameObject);
        life--;

        if (life > 0)
        {
            return;
        }

        Destroy(this.gameObject);
    }
}
