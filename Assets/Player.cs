﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] float life;
    [SerializeField] float speed;
    [SerializeField] float sensitivity;
    [SerializeField] Rigidbody rigidbody;
    [SerializeField] Camera camera;
    [SerializeField] Transform muzzle;
    [SerializeField] GameObject bullet;

    Vector3 cameraAngles;
    float fireTimer;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        cameraAngles += new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0f) * sensitivity;
        cameraAngles.x = Mathf.Clamp(cameraAngles.x, -90f, 90f);

        camera.transform.localEulerAngles = new Vector3(cameraAngles.x, 0f, 0f);
        this.transform.eulerAngles = new Vector3(0f, cameraAngles.y, 0f);

        Vector3 velocity = (this.transform.forward * Input.GetAxis("Vertical") + camera.transform.right * Input.GetAxis("Horizontal")) * speed;
        rigidbody.velocity = new Vector3(velocity.x, rigidbody.velocity.y, velocity.z);

        fireTimer += Time.deltaTime;
        if (Input.GetButton("Fire1") && fireTimer > 0.1f)
        {
            fireTimer = 0f;

            GameObject go = Instantiate(bullet);
            go.tag = "PlayerBullet";
            go.transform.position = muzzle.position;
            go.GetComponent<Rigidbody>().velocity += muzzle.forward * 30f;
            Destroy(go, 5f);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != "EnemyBullet")
        {
            return;
        }

        Destroy(collision.collider.gameObject);
        life--;

        if (life > 0)
        {
            return;
        }

        SceneManager.LoadScene("Title");
    }
}
