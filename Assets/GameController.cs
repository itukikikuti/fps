﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] GameObject enemy;
    float enemyTimer;
    float enemyInterval = 3f;

    void Update()
    {
        enemyTimer += Time.deltaTime;
        if (enemyTimer > enemyInterval)
        {
            enemyInterval *= 0.99f;
            Debug.Log(enemyInterval);
            enemyTimer = 0f;
            GameObject go = Instantiate(enemy);
            go.transform.position = new Vector3(Random.Range(-10f, 10f), 5f, Random.Range(-10f, 10f));
        }
    }
}
